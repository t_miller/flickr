# Flickr Photo Stream

An application to show the photos from flickr and their descriptions.
The user can search flickr photo's title, description and / or tags through the search input.

This is a single page application built with React and styled components. 
Using cross-fetch to fetch the data.


## Development

To keep the height of the photo items more consistant the descriptions have been truncated if they are over a certain length.

To improve the speed I create a image component which would have a placeholder svg and render the image onload.

A future feature for the search functionallity would be to take the search query and add them to a list using the tag component. 
This would allow the user to have multiple queries.

To make the site more personalised you could introcude a form of state managment (Redux) or local storage and save the users search.


## Installation and Setup


You will need node.js and npm installed globally on your machine.

### steps

	- Clone this repository
	
	- Run "npm install" to install the packages
	
	- Run "npm start"
	
	- Visit localhost:3000
	


