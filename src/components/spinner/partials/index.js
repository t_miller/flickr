import styled, { css } from "styled-components";
import { spinner } from "../../theme/keyframes";

export const StyledContainer = styled.div`
  width: 6rem;
  height: 6rem;
  position: relative;
  display: flex;
  justify-content: center;
  vertical-align: middle;
  align-items: center;
`;

export const StyledSpinner = styled.div`
  ${props =>
    css`
      border: 3px solid ${props.theme.colors.primary};
      border-top-color: ${props.theme.colors.cta};
      width: ${props.size}%;
      height: ${props.size}%;
      animation: ${spinner} 3s ease-in-out ${props.delay}s infinite;
    `};
  border-radius: 50%;
  position: absolute;
  margin: 1rem;
`;
