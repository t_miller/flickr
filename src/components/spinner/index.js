import React from "react";

import { StyledContainer, StyledSpinner } from "./partials";

const Spinner = () => (
  <StyledContainer>
    <StyledSpinner size={100} delay={0}></StyledSpinner>
    <StyledSpinner size={80} delay={1}></StyledSpinner>
    <StyledSpinner size={60} delay={2}></StyledSpinner>
  </StyledContainer>
);

export default Spinner;
