import { keyframes } from "styled-components";

export const shake = keyframes`
2%, 18% {
    transform: translate3d(-5px, 0, 0);
}
6%, 10%, 14% {
    transform: translate3d(-5px, 0, 0);
}
18% {
    transform: translate3d(0px, 0, 0);
}`;

export const spinner = keyframes`
33.333% {
    transform: rotate(360deg);
  }
  100% {
    transform: rotate(360deg);
  }`;
