import React from "react";
import { ThemeProvider, createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  body {
    background: ${props => props.theme.colors.brandBackground};
    margin: 0;
    color: ${props => props.theme.colors.light};
}
*, *:before, *:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    outline: none;
  }
`;

const variables = {
  fontFamily: "Open Sans",
  borderRadius: "5px"
};

const colors = {
  primary: "#365369",
  white: "#FFF",
  brandBackground: "#F1F5F9",
  backgroundLight: "rgba(44,44,44,.05)",
  cta: "#73EBD1",
  dark: "rgb(0,0,0)",
  dark05: "rgba(0,0,0,0.05)",
  dark10: "rgba(0,0,0,0.1)",
  dark20: "rgba(0,0,0,0.2)",
  dark40: "rgba(0,0,0,0.4)",
  dark60: "rgba(0,0,0,0.6)",
  dark80: "rgba(0,0,0,0.8)",
  gray: "rgba(44,44,44,.6)",
  red: "rgba(255, 0, 0, 0.8);",
  blue: "#3333FF"
};

const breakPoints = {
  wide: "1200px",
  desktop: "960px",
  tablet: "768px",
  mobile: "414px"
};

const theme = {
  colors: {
    ...colors
  },
  breakPoints: {
    ...breakPoints
  },
  fonts: ["sans-serif", "Roboto"],
  fontSizes: {
    small: "1em",
    medium: "2em",
    large: "3em"
  },
  typography: {
    heading: `
              font-family: ${variables.fontFamily};
              font-weight: 700;
              font-size: 36px;
              line-height: 1.6;
              color: ${colors.primary};
              @media (max-width: ${breakPoints.tablet}){
                  font-size: 32px;
              }
              @media (max-width: ${breakPoints.mobile}){
                font-size: 28px;
            }
          `,
    bodyLarge: `font-family: ${variables.fontFamily};
              font-weight: 700;
              font-size: 18px;
              line-height: 1.6;
              color: ${colors.primary};
              @media (max-width: ${breakPoints.tablet}){
                  font-size: 16px;
              }
          `,
    body: `
              font-family: ${variables.fontFamily};
              font-weight: 400;
              font-size: 16px;
              line-height: 1.6;
              color: ${colors.primary};
              @media (max-width: ${breakPoints.tablet}){
                  font-size: 14px;
              }
          `,
    bodySmall: `
          font-family: ${variables.fontFamily};
          font-weight: 400;
          font-size: 14px;
          line-height: 1.6;
          color: ${colors.primary};
          @media (max-width: ${breakPoints.tablet}){
              font-size: 12px;
          }
      `
  }
};

const Theme = ({ children }) => (
  <ThemeProvider theme={theme}>
    <GlobalStyle />
    {children}
  </ThemeProvider>
);

export default Theme;
