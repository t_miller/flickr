import React from "react";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Theme from "./";

test("Theme renders children", () => {
  const testText = "Test";
  render(
    <Theme>
      <span id={"spanEl"}>{testText}</span>
    </Theme>
  );

  expect(screen.getByText(testText)).toBeInTheDocument();
});
