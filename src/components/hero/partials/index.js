import styled from "styled-components";

export const StyledHero = styled.div`
  & h1 {
    ${props => props.theme.typography.heading};
  }
`;
