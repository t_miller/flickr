import React from "react";

import { StyledHero } from "./partials";

const Hero = ({ title }) => {
  return (
    <StyledHero>
      <h1>{title}</h1>
    </StyledHero>
  );
};

export default Hero;
