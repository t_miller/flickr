import styled, { css } from "styled-components";
import { shake } from "../../../theme/keyframes";

export const StyledContainer = styled.div`
  width: calc(25% - 1.5rem);
  background: ${props => props.theme.colors.white};
  box-shadow: 0px 3px 6px #20212129;
  padding: 1rem;
  margin: 1rem;

  &:first-of-type ,:nth-of-type(4n + 5) {
    margin: 1rem 1rem 1rem 0;
  }

  &:nth-of-type(4n) {
    margin: 1rem 0 1rem 1rem;
  }

  @media (max-width: calc(${props => props.theme.breakPoints.desktop})) {
    width: calc(33.33% - 1rem);
    
    &:first-of-type , &:nth-of-type(3n + 4) {
        margin: 1rem 1rem 1rem 0;
    }
    
    &:nth-of-type(3n + 2) {
        margin: 0.5rem;
      }
  
    &:nth-of-type(3n) {
      margin: 1rem 0 1rem 1rem;
    }
  
  }
  
  @media (max-width: calc(${props => props.theme.breakPoints.tablet})) {
    width: calc(50% - 0.5rem);

    &:first-of-type ,:nth-of-type(2n + 3) {
        margin: 0.5rem 0.5rem 0.5rem 0;
      }
    
      &:nth-of-type(2n) {
        margin: 0.5rem 0 0.5rem 0.5rem;
      }
  }

  @media (max-width: calc(${props => props.theme.breakPoints.mobile})) {
    width: 100%;

    &:nth-of-type(1n) {
        margin: 0.5rem 0;
    }
  }
`;

export const StyledImg = styled.div`
  display: flex;
  justify-content: center;
  img {
    max-height: 15rem;
    width: auto;
    max-width: 100%;
  }
`;

export const StyledHeader = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin: 1rem 0;

  & > a {
    & > h2 {
      overflow: hidden;
      overflow-wrap: break-word;
      word-break: break-word;
      width: 100%;
      ${props => props.theme.typography.bodyLarge}
      margin: 0;
    }
  }

  & > h3 {
    overflow: hidden;
    margin: 0;
    & > a {
      ${props => props.theme.typography.body}
      word-break: break-word;
    }
  }
`;

export const StyledSpan = styled.span`
  ${props => `${props.theme.typography.bodySmall}
  ${
    props.subTitle
      ? "padding: 0.2rem 0.4rem; align-items: center; text-align: center; display: flex;"
      : "word-break: break-word;"
  }`}
`;

export const StyledBtn = styled.div`
  margin-top: 0.5rem;

  & button {
    ${props => `${props.theme.typography.body}
    color: ${props.theme.colors.blue};
    `}
    border: none;
    background: transparent;
    padding: 0;
    cursor: pointer;
  }
`;

export const StyledSection = styled.div`
  margin: 0.5rem 0;
  ${props => (props.flex ? "display: flex;" : "")}
`;

export const StyledTags = styled.div`
  display: flex;
  overflow-x: auto;
  overflow-y: hidden;
  scroll-snap-type: x mandatory;
  align-items: center;
  width: auto;

  -ms-overflow-style: none;
  scrollbar-width: none;
  &::-webkit-scrollbar {
    display: none;
  }

  ${props =>
    props.shake
      ? css`
          animation: ${shake} 6s linear 2s infinite;
        `
      : ""}
`;
