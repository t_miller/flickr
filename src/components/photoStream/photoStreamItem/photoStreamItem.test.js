import React from "react";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { Theme } from "../../";
import PhotoStreamItem from "./";

const propsObj = {
  description: { _content: "Sellafield, Cumbria" },
  farm: 66,
  height_n: 240,
  height_z: 480,
  id: "51282053782",
  isfamily: 0,
  isfriend: 0,
  ispublic: 1,
  owner: "47536577@N00",
  ownername: "Tom Bolton",
  secret: "e44296734f",
  server: "65535",
  tags: "",
  title: "Sellafield view",
  url_n: "https://live.staticflickr.com/65535/51282053782_e44296734f_n.jpg",
  url_z: "https://live.staticflickr.com/65535/51282053782_e44296734f_z.jpg",
  width_n: 320
};

describe("PhotoStreamItem", () => {
  test("renders stream item component", async () => {
    render(
      <Theme>
        <PhotoStreamItem {...propsObj} />
      </Theme>
    );
    expect(screen.getByText("Tom Bolton")).toBeInTheDocument();
  });
});
