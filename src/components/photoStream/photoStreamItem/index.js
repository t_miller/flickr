import React, { useState } from "react";
import parse from "html-react-parser";
import LazyLoad from "react-lazy-load";
import { Tag } from "../../";
import {
  StyledContainer,
  StyledImg,
  StyledHeader,
  StyledSpan,
  StyledBtn,
  StyledSection,
  StyledTags
} from "./partials";

const PhotoStreamItem = ({
  ownername,
  url_n,
  owner,
  description,
  id,
  tags,
  title
}) => {
  const [readMore, SetReadMore] = useState(false);

  const readMoreHandler = () => {
    SetReadMore(!readMore);
  };

  return (
    <StyledContainer>
      <StyledImg>
        <LazyLoad offsetBottom={300}>
          <img src={url_n} alt={ownername} />
        </LazyLoad>
      </StyledImg>
      <StyledHeader>
        {title && (
          <a href={`https://www.flickr.com/photos/${owner}/${id}`}>
            <h2>{title}</h2>
          </a>
        )}
        <h3>
          <StyledSpan>&nbsp;by&nbsp;</StyledSpan>
          <a href={`https://www.flickr.com/people/${owner}/`}>{ownername}</a>
        </h3>
      </StyledHeader>
      <StyledSection>
        {description?._content &&
          description._content.trim() &&
          (description._content.length < 60 || readMore ? (
            <StyledSpan>
              Description:&nbsp;
              {parse(description._content)}
              {description._content.length > 60 && (
                <StyledBtn>
                  <button onClick={readMoreHandler}>Show less...</button>
                </StyledBtn>
              )}
            </StyledSpan>
          ) : (
            <StyledSpan>
              Description:&nbsp;
              {parse(description._content.substring(0, 60))}
              <StyledBtn>
                <button onClick={readMoreHandler}>Read more...</button>
              </StyledBtn>
            </StyledSpan>
          ))}
      </StyledSection>
      {tags && (
        <StyledSection flex>
          <StyledSpan subTitle>Tags:&nbsp;</StyledSpan>
          <StyledTags shake={tags.split(" ").length > 3}>
            {tags.split(" ").map((e, i) => (
              <Tag key={i} label={e} />
            ))}
          </StyledTags>
        </StyledSection>
      )}
    </StyledContainer>
  );
};
export default PhotoStreamItem;
