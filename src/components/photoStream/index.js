import React from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import PhotoStreamItem from "./photoStreamItem";
import { Spinner } from "../";
import { StyledSpinner } from "./partials";

const PhotoStream = ({ flickrData = [], fetchNextPage }) => {
  return (
    <InfiniteScroll
      dataLength={flickrData}
      next={fetchNextPage}
      style={{ display: "flex", flexWrap: "wrap" }}
      hasMore={true}
      loader={
        <StyledSpinner>
          <Spinner />
        </StyledSpinner>
      }
    >
      {flickrData &&
        flickrData.length > 0 &&
        flickrData.map((e, i) => <PhotoStreamItem key={i} {...e} />)}
    </InfiniteScroll>
  );
};

export default PhotoStream;
