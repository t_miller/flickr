import styled from "styled-components";

export const StyledSpinner = styled.div`
  display: flex;
  justify-content: center;
  margin: 2rem 0;
  width: 100%;
`;
