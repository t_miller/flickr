import "@testing-library/jest-dom/extend-expect";
import React from "react";
import { render, waitFor, screen } from "@testing-library/react";
import PhotoStream from "./";
import { fetchFlickrData } from "./fetchFlickrData";
import { Theme } from "../";

const mockedData = {
  description: { _content: "Sellafield, Cumbria" },
  farm: 66,
  height_n: 240,
  height_z: 480,
  id: "51282053782",
  isfamily: 0,
  isfriend: 0,
  ispublic: 1,
  owner: "47536577@N00",
  ownername: "Tom Bolton",
  secret: "e44296734f",
  server: "65535",
  tags: "",
  title: "Sellafield view",
  url_n: "https://live.staticflickr.com/65535/51282053782_e44296734f_n.jpg",
  url_z: "https://live.staticflickr.com/65535/51282053782_e44296734f_z.jpg",
  width_n: 320
};
jest.mock("./fetchFlickrData");

afterEach(() => {
  jest.resetAllMocks();
});

test("PhotoStream loads and renders item", async () => {
  await fetchFlickrData.mockResolvedValueOnce(() => [mockedData]);

  render(
    <Theme>
      <PhotoStream searchQuery="Tom" />
    </Theme>
  );

  const spinnerEl = screen.queryByText("Loading...");
  await expect(spinnerEl).toBeNull();

  await waitFor(() => expect(fetchFlickrData).toHaveBeenCalledTimes(1));

  await waitFor(() =>
    expect(screen.getByText(mockedData.ownername)).toBeInTheDocument()
  );

  expect(await screen.getByText(mockedData.title)).toBeInTheDocument();
});
