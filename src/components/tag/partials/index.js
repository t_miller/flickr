import styled from "styled-components";

export const StyledTag = styled.div`
  padding: 0.2rem 0.4rem;
  width: fit-content;
  margin-right: 1rem;
  background: ${props => props.theme.colors.backgroundLight};
  & span {
    ${props => `${props.theme.typography.body}
    color: ${props.theme.colors.gray}`}
  }
`;
