import React from "react";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { Theme, Tag } from "../";

test("Tag renders text", () => {
  const testText = "Tag label";
  render(
    <Theme>
      <Tag label={testText} />
    </Theme>
  );

  expect(screen.getByText(testText)).toBeInTheDocument();
});
