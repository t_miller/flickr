import React from "react";
import { StyledTag } from "./partials";

const Tag = ({ label = "" }) => (
  <StyledTag>
    <span>{label}</span>
  </StyledTag>
);

export default Tag;
