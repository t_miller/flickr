import styled from "styled-components";

export const StyledContainer = styled.div`
  box-shadow: 0px 3px 6px #20212129;
  background: ${props => props.theme.colors.white};
  width: 33.33%;

  @media (max-width: calc(${props => props.theme.breakPoints.tablet})) {
    width: 50%;
  }

  @media (max-width: calc(${props => props.theme.breakPoints.mobile})) {
    width: 100%;
  }
`;

export const StyledInput = styled.input`
  padding: 0 0.5rem 0.5rem 0.5rem;
  ${props => `${props.theme.typography.body}
  background: ${props.theme.colors.white};`}
  border: none;
  width: 100%;
`;

export const StyledLabel = styled.div`
  margin-left: 1rem;
  & {
    ${props => props.theme.typography.bodySmall}
  }
`;
