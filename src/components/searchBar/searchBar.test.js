import React, { useRef } from "react";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { Theme, SearchBar } from "../";

test("SearchBar takes value", () => {
  const testText = "Test";
  const testLabel = "label";

  render(
    <Theme>
      <SearchBar value={testText} onChange={() => ""} label={testLabel} />
    </Theme>
  );

  expect(screen.getByText(testLabel)).toBeInTheDocument();
  expect(screen.getByDisplayValue(testText)).toBeInTheDocument();
});
