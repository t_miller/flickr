import React from "react";
import { StyledContainer, StyledInput, StyledLabel } from "./partials";

const SearchBar = ({ label = "", onChange, value = "" }) => {
  return (
    <StyledContainer>
      <StyledLabel>
        <span>{label}</span>
      </StyledLabel>
      <StyledInput type="text" onChange={onChange} value={value} />
    </StyledContainer>
  );
};

export default SearchBar;
