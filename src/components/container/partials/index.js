import styled from "styled-components"

export const StyledContainer = styled.div`
    width: 100%;
    margin: 0 auto;
    max-width: ${props => props.theme.breakPoints.wide};
    @media (max-width: calc(${props => props.theme.breakPoints.wide} + 64px)){
        max-width: ${props => props.theme.breakPoints.desktop};
    }
    @media (max-width: calc(${props => props.theme.breakPoints.desktop} + 64px)){
        padding: 0 32px;
    }
    @media (max-width: ${props => props.theme.breakPoints.tablet}){
        max-width: 100%;
        padding: 0 24px;
    }
    @media (max-width: ${props => props.theme.breakPoints.mobile}){
        padding: 0 16px;
    }
`