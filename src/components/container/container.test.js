import React from "react";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { Theme, Container } from "../";

test("Container renders children", () => {
  const testText = "Test";
  render(
    <Theme>
      <Container>
        <span id={"spanEl"}>{testText}</span>
      </Container>
    </Theme>
  );

  expect(screen.getByText(testText)).toBeInTheDocument();
});
