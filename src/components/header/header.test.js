import React from "react";
import "@testing-library/jest-dom";
import { render, component } from "@testing-library/react";
import { Theme, Header } from "../";

test("Header renders", () => {
  const { container } = render(
    <Theme>
      <Header />
    </Theme>
  );

  expect(container.querySelector("header")).toBeInTheDocument();
});
