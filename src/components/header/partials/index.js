import styled from "styled-components";

export const StyledHeader = styled.header`
  height: 8vh;
  width: 100%;
  background: ${props => props.theme.colors.white};
`;
