import styled from "styled-components";

export const StyledSection = styled.div`
  margin: 2rem 0;

  ${props => (props.flex ? "display: flex; justify-content: center;" : "")}
`;
