import fetch from "cross-fetch";

export const fetchFlickrData = (page = 1, searchQuery) => {
  const extrasParam = "extras=description,tags,url_n,url_z,owner_name";
  return fetch(
    `https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=c42d652f89b4f7e3b5c417f30c8154e2&${
      searchQuery ? `text=${searchQuery}` : "max_taken_date=01-01-2000"
    }&safe_search=1&${extrasParam}&format=json&nojsoncallback=1&per_page=20&page=${page}`
  )
    .then(res => res.json())
    .then(json => {
      return json?.photos?.photo ? json.photos.photo : [];
    })
    .catch(err => console.log("Fetch error ", err));
};
