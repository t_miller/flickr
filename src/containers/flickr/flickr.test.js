import React from "react";
import "@testing-library/jest-dom";
import { render, waitFor, screen, fireEvent } from "@testing-library/react";
import { fetchFlickrData } from "../../components/photoStream/fetchFlickrData";
import { Theme } from "../../components";
import Flickr from "./";

const mockedData = {
  description: { _content: "Sellafield, Cumbria" },
  farm: 66,
  height_n: 240,
  height_z: 480,
  id: "51282053782",
  isfamily: 0,
  isfriend: 0,
  ispublic: 1,
  owner: "47536577@N00",
  ownername: "Tom Bolton",
  secret: "e44296734f",
  server: "65535",
  tags: "",
  title: "Sellafield view",
  url_n: "https://live.staticflickr.com/65535/51282053782_e44296734f_n.jpg",
  url_z: "https://live.staticflickr.com/65535/51282053782_e44296734f_z.jpg",
  width_n: 320
};
jest.mock("../../components/photoStream/fetchFlickrData");

afterEach(() => {
  jest.resetAllMocks();
});

test("Flickr renders components", async () => {
  fetchFlickrData.mockResolvedValueOnce(() => [mockedData]);
  const testInput = "Test input";

  render(
    <Theme>
      <Flickr />
    </Theme>
  );

  expect(screen.getByText("Flickr Photo Stream")).toBeInTheDocument();

  await waitFor(() => expect(fetchFlickrData).toHaveBeenCalledTimes(1));

  expect(await screen.getByText(mockedData.ownername)).toBeInTheDocument();
  expect(await screen.getByText(mockedData.title)).toBeInTheDocument();

  fireEvent.change(screen.getByRole("textbox"), {
    target: { value: testInput }
  });

  expect(screen.getByDisplayValue(testInput)).toBeInTheDocument();
});
