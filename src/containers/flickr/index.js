import React, { useState, useEffect } from "react";
import { Container, PhotoStream, SearchBar, Hero } from "../../components";
import { fetchFlickrData } from "./fetchFlickrData";
import { StyledSection } from "./partials";

const useDebounce = (value, delay) => {
  const [debouncedValue, setDebouncedValue] = useState(value);
  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);
    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);
  return debouncedValue;
};

const useFlickrData = (page = 1, searchQuery = "") => {
  const [flickrData, SetFlickrData] = useState([]);
  useEffect(() => {
    const CallFetchFlickrData = async () => {
      const res = await fetchFlickrData(page, searchQuery);
      SetFlickrData(prevData => (page === 1 ? res : [...prevData, ...res]));
    };

    CallFetchFlickrData();
  }, [searchQuery, page]);
  return flickrData;
};

const Flickr = () => {
  const [state, SetState] = useState({
    userInput: "",
    queryPage: 1
  });
  const debounceQuery = useDebounce(state, 1000);
  const flickrData = useFlickrData(
    debounceQuery.queryPage,
    debounceQuery.userInput
  );

  const seachHandler = e => {
    const { value } = e.target;
    SetState({ userInput: value, queryPage: 1 });
  };

  const fetchNextPage = () => {
    SetState({ userInput: state.userInput, queryPage: state.queryPage + 1 });
  };

  return (
    <Container>
      <StyledSection>
        <Hero title="Flickr Photo Stream" />
      </StyledSection>
      <StyledSection flex>
        <SearchBar
          value={state.userInput}
          onChange={seachHandler}
          label="Search"
        />
      </StyledSection>
      <StyledSection>
        <PhotoStream flickrData={flickrData} fetchNextPage={fetchNextPage} />
      </StyledSection>
    </Container>
  );
};

export default Flickr;

// const useFlickrData = (searchQuery = "") => {
//   // const [flickrData, SetFlickrData] = useState([]);
//   const [state, SetState] = useState({
//     prevQuery: "",
//     page: 1,
//     flickrData: []
//   });
//   useEffect(() => {
//     const CallFetchFlickrData = async () => {
//       // const flickrDataRes = await fetchFlickrData(state.page, searchQuery);

//       if (state.prevQuery === searchQuery) {
//         SetState({
//           prevQuery: searchQuery,
//           page: state.page + 1,
//           flickrData: await fetchFlickrData(state.page, searchQuery)
//         });
//       } else {
//         SetState({
//           prevQuery: searchQuery,
//           page: 1,
//           flickrData: await fetchFlickrData(1, searchQuery)
//         });
//       }
//     };
//     // SetFlickrData(
//     //   page === 1 ? flickrDataRes : [...flickrData, flickrDataRes]
//     // );
//     // };
//     CallFetchFlickrData();
//   }, [searchQuery]);
//   return state.flickrData;
// };
