import { Theme, Header } from "./components";
import Flickr from "./containers/flickr";

const App = () => {
  return (
    <Theme>
      <Header />
      <Flickr />
    </Theme>
  );
};

export default App;
